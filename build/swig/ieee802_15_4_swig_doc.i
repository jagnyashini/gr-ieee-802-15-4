
/*
 * This file was automatically generated using swig_doc.py.
 *
 * Any changes to it will be lost next time it is regenerated.
 */




%feature("docstring") gr::ieee802_15_4::access_code_prefixer "

Constructor Specific Documentation:



Args:
    pad : 
    preamble : "

%feature("docstring") gr::ieee802_15_4::access_code_prefixer::make "

Constructor Specific Documentation:



Args:
    pad : 
    preamble : "

%feature("docstring") gr::ieee802_15_4::access_code_removal_b "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::access_code_removal_b.

To avoid accidental use of raw pointers, ieee802_15_4::access_code_removal_b's constructor is in a private implementation class. ieee802_15_4::access_code_removal_b::make is the public interface for creating new instances.

Args:
    len_payload : "

%feature("docstring") gr::ieee802_15_4::access_code_removal_b::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::access_code_removal_b.

To avoid accidental use of raw pointers, ieee802_15_4::access_code_removal_b's constructor is in a private implementation class. ieee802_15_4::access_code_removal_b::make is the public interface for creating new instances.

Args:
    len_payload : "

%feature("docstring") gr::ieee802_15_4::chips_to_bits_fb "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::chips_to_bits_fb.

To avoid accidental use of raw pointers, ieee802_15_4::chips_to_bits_fb's constructor is in a private implementation class. ieee802_15_4::chips_to_bits_fb::make is the public interface for creating new instances.

Args:
    chip_seq : "

%feature("docstring") gr::ieee802_15_4::chips_to_bits_fb::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::chips_to_bits_fb.

To avoid accidental use of raw pointers, ieee802_15_4::chips_to_bits_fb's constructor is in a private implementation class. ieee802_15_4::chips_to_bits_fb::make is the public interface for creating new instances.

Args:
    chip_seq : "

%feature("docstring") gr::ieee802_15_4::codeword_demapper_ib "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::codeword_demapper_ib.

To avoid accidental use of raw pointers, ieee802_15_4::codeword_demapper_ib's constructor is in a private implementation class. ieee802_15_4::codeword_demapper_ib::make is the public interface for creating new instances.

Args:
    bits_per_cw : 
    codewords : "

%feature("docstring") gr::ieee802_15_4::codeword_demapper_ib::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::codeword_demapper_ib.

To avoid accidental use of raw pointers, ieee802_15_4::codeword_demapper_ib's constructor is in a private implementation class. ieee802_15_4::codeword_demapper_ib::make is the public interface for creating new instances.

Args:
    bits_per_cw : 
    codewords : "

%feature("docstring") gr::ieee802_15_4::codeword_mapper_bi "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::codeword_mapper_bi.

To avoid accidental use of raw pointers, ieee802_15_4::codeword_mapper_bi's constructor is in a private implementation class. ieee802_15_4::codeword_mapper_bi::make is the public interface for creating new instances.

Args:
    bits_per_cw : 
    codewords : "

%feature("docstring") gr::ieee802_15_4::codeword_mapper_bi::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::codeword_mapper_bi.

To avoid accidental use of raw pointers, ieee802_15_4::codeword_mapper_bi's constructor is in a private implementation class. ieee802_15_4::codeword_mapper_bi::make is the public interface for creating new instances.

Args:
    bits_per_cw : 
    codewords : "

%feature("docstring") gr::ieee802_15_4::codeword_soft_demapper_fb "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::codeword_soft_demapper_fb.

To avoid accidental use of raw pointers, ieee802_15_4::codeword_soft_demapper_fb's constructor is in a private implementation class. ieee802_15_4::codeword_soft_demapper_fb::make is the public interface for creating new instances.

Args:
    bits_per_cw : 
    codewords : "

%feature("docstring") gr::ieee802_15_4::codeword_soft_demapper_fb::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::codeword_soft_demapper_fb.

To avoid accidental use of raw pointers, ieee802_15_4::codeword_soft_demapper_fb's constructor is in a private implementation class. ieee802_15_4::codeword_soft_demapper_fb::make is the public interface for creating new instances.

Args:
    bits_per_cw : 
    codewords : "

%feature("docstring") gr::ieee802_15_4::deinterleaver_ff "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::deinterleaver_ff.

To avoid accidental use of raw pointers, ieee802_15_4::deinterleaver_ff's constructor is in a private implementation class. ieee802_15_4::deinterleaver_ff::make is the public interface for creating new instances.

Args:
    intlv_seq : "

%feature("docstring") gr::ieee802_15_4::deinterleaver_ff::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::deinterleaver_ff.

To avoid accidental use of raw pointers, ieee802_15_4::deinterleaver_ff's constructor is in a private implementation class. ieee802_15_4::deinterleaver_ff::make is the public interface for creating new instances.

Args:
    intlv_seq : "

%feature("docstring") gr::ieee802_15_4::dqcsk_demapper_cc "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::dqcsk_demapper_cc.

To avoid accidental use of raw pointers, ieee802_15_4::dqcsk_demapper_cc's constructor is in a private implementation class. ieee802_15_4::dqcsk_demapper_cc::make is the public interface for creating new instances.

Args:
    chirp_seq : 
    time_gap_1 : 
    time_gap_2 : 
    len_subchirp : 
    num_subchirps : "

%feature("docstring") gr::ieee802_15_4::dqcsk_demapper_cc::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::dqcsk_demapper_cc.

To avoid accidental use of raw pointers, ieee802_15_4::dqcsk_demapper_cc's constructor is in a private implementation class. ieee802_15_4::dqcsk_demapper_cc::make is the public interface for creating new instances.

Args:
    chirp_seq : 
    time_gap_1 : 
    time_gap_2 : 
    len_subchirp : 
    num_subchirps : "

%feature("docstring") gr::ieee802_15_4::dqcsk_mapper_fc "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::dqcsk_mapper_fc.

To avoid accidental use of raw pointers, ieee802_15_4::dqcsk_mapper_fc's constructor is in a private implementation class. ieee802_15_4::dqcsk_mapper_fc::make is the public interface for creating new instances.

Args:
    chirp_seq : 
    time_gap_1 : 
    time_gap_2 : 
    len_subchirp : 
    num_subchirp : 
    nsym_frame : "

%feature("docstring") gr::ieee802_15_4::dqcsk_mapper_fc::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::dqcsk_mapper_fc.

To avoid accidental use of raw pointers, ieee802_15_4::dqcsk_mapper_fc's constructor is in a private implementation class. ieee802_15_4::dqcsk_mapper_fc::make is the public interface for creating new instances.

Args:
    chirp_seq : 
    time_gap_1 : 
    time_gap_2 : 
    len_subchirp : 
    num_subchirp : 
    nsym_frame : "

%feature("docstring") gr::ieee802_15_4::dqpsk_mapper_ff "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::dqpsk_mapper_ff.

To avoid accidental use of raw pointers, ieee802_15_4::dqpsk_mapper_ff's constructor is in a private implementation class. ieee802_15_4::dqpsk_mapper_ff::make is the public interface for creating new instances.

Args:
    framelen : 
    forward : "

%feature("docstring") gr::ieee802_15_4::dqpsk_mapper_ff::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::dqpsk_mapper_ff.

To avoid accidental use of raw pointers, ieee802_15_4::dqpsk_mapper_ff's constructor is in a private implementation class. ieee802_15_4::dqpsk_mapper_ff::make is the public interface for creating new instances.

Args:
    framelen : 
    forward : "

%feature("docstring") gr::ieee802_15_4::dqpsk_soft_demapper_cc "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::dqpsk_soft_demapper_cc.

To avoid accidental use of raw pointers, ieee802_15_4::dqpsk_soft_demapper_cc's constructor is in a private implementation class. ieee802_15_4::dqpsk_soft_demapper_cc::make is the public interface for creating new instances.

Args:
    framelen : "

%feature("docstring") gr::ieee802_15_4::dqpsk_soft_demapper_cc::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::dqpsk_soft_demapper_cc.

To avoid accidental use of raw pointers, ieee802_15_4::dqpsk_soft_demapper_cc's constructor is in a private implementation class. ieee802_15_4::dqpsk_soft_demapper_cc::make is the public interface for creating new instances.

Args:
    framelen : "

%feature("docstring") gr::ieee802_15_4::frame_buffer_cc "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::frame_buffer_cc.

To avoid accidental use of raw pointers, ieee802_15_4::frame_buffer_cc's constructor is in a private implementation class. ieee802_15_4::frame_buffer_cc::make is the public interface for creating new instances.

Args:
    nsym_frame : "

%feature("docstring") gr::ieee802_15_4::frame_buffer_cc::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::frame_buffer_cc.

To avoid accidental use of raw pointers, ieee802_15_4::frame_buffer_cc's constructor is in a private implementation class. ieee802_15_4::frame_buffer_cc::make is the public interface for creating new instances.

Args:
    nsym_frame : "

%feature("docstring") gr::ieee802_15_4::interleaver_ii "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::interleaver_ii.

To avoid accidental use of raw pointers, ieee802_15_4::interleaver_ii's constructor is in a private implementation class. ieee802_15_4::interleaver_ii::make is the public interface for creating new instances.

Args:
    intlv_seq : 
    forward : "

%feature("docstring") gr::ieee802_15_4::interleaver_ii::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::interleaver_ii.

To avoid accidental use of raw pointers, ieee802_15_4::interleaver_ii's constructor is in a private implementation class. ieee802_15_4::interleaver_ii::make is the public interface for creating new instances.

Args:
    intlv_seq : 
    forward : "

%feature("docstring") gr::ieee802_15_4::mac "This is the MAC Block.

The MAC block...

Constructor Specific Documentation:



Args:
    debug : 
    fcf : 
    seq_nr : 
    dst_pan : 
    dst : 
    src : "







%feature("docstring") gr::ieee802_15_4::mac::make "This is the MAC Block.

The MAC block...

Constructor Specific Documentation:



Args:
    debug : 
    fcf : 
    seq_nr : 
    dst_pan : 
    dst : 
    src : "

%feature("docstring") gr::ieee802_15_4::multiuser_chirp_detector_cc "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::multiuser_chirp_detector_cc.

To avoid accidental use of raw pointers, ieee802_15_4::multiuser_chirp_detector_cc's constructor is in a private implementation class. ieee802_15_4::multiuser_chirp_detector_cc::make is the public interface for creating new instances.

Args:
    chirp_seq : 
    time_gap_1 : 
    time_gap_2 : 
    len_subchirp : 
    threshold : "

%feature("docstring") gr::ieee802_15_4::multiuser_chirp_detector_cc::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::multiuser_chirp_detector_cc.

To avoid accidental use of raw pointers, ieee802_15_4::multiuser_chirp_detector_cc's constructor is in a private implementation class. ieee802_15_4::multiuser_chirp_detector_cc::make is the public interface for creating new instances.

Args:
    chirp_seq : 
    time_gap_1 : 
    time_gap_2 : 
    len_subchirp : 
    threshold : "

%feature("docstring") gr::ieee802_15_4::packet_sink "

Constructor Specific Documentation:



Args:
    threshold : "

%feature("docstring") gr::ieee802_15_4::packet_sink::make "

Constructor Specific Documentation:



Args:
    threshold : "

%feature("docstring") gr::ieee802_15_4::phr_prefixer "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::phr_prefixer.

To avoid accidental use of raw pointers, ieee802_15_4::phr_prefixer's constructor is in a private implementation class. ieee802_15_4::phr_prefixer::make is the public interface for creating new instances.

Args:
    phr : "

%feature("docstring") gr::ieee802_15_4::phr_prefixer::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::phr_prefixer.

To avoid accidental use of raw pointers, ieee802_15_4::phr_prefixer's constructor is in a private implementation class. ieee802_15_4::phr_prefixer::make is the public interface for creating new instances.

Args:
    phr : "

%feature("docstring") gr::ieee802_15_4::phr_removal "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::phr_removal.

To avoid accidental use of raw pointers, ieee802_15_4::phr_removal's constructor is in a private implementation class. ieee802_15_4::phr_removal::make is the public interface for creating new instances.

Args:
    phr : "

%feature("docstring") gr::ieee802_15_4::phr_removal::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::phr_removal.

To avoid accidental use of raw pointers, ieee802_15_4::phr_removal's constructor is in a private implementation class. ieee802_15_4::phr_removal::make is the public interface for creating new instances.

Args:
    phr : "

%feature("docstring") gr::ieee802_15_4::preamble_sfd_prefixer_ii "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::preamble_sfd_prefixer_ii.

To avoid accidental use of raw pointers, ieee802_15_4::preamble_sfd_prefixer_ii's constructor is in a private implementation class. ieee802_15_4::preamble_sfd_prefixer_ii::make is the public interface for creating new instances.

Args:
    preamble : 
    sfd : 
    nsym_frame : "

%feature("docstring") gr::ieee802_15_4::preamble_sfd_prefixer_ii::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::preamble_sfd_prefixer_ii.

To avoid accidental use of raw pointers, ieee802_15_4::preamble_sfd_prefixer_ii's constructor is in a private implementation class. ieee802_15_4::preamble_sfd_prefixer_ii::make is the public interface for creating new instances.

Args:
    preamble : 
    sfd : 
    nsym_frame : "

%feature("docstring") gr::ieee802_15_4::preamble_tagger_cc "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::preamble_tagger_cc.

To avoid accidental use of raw pointers, ieee802_15_4::preamble_tagger_cc's constructor is in a private implementation class. ieee802_15_4::preamble_tagger_cc::make is the public interface for creating new instances.

Args:
    len_preamble : "

%feature("docstring") gr::ieee802_15_4::preamble_tagger_cc::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::preamble_tagger_cc.

To avoid accidental use of raw pointers, ieee802_15_4::preamble_tagger_cc's constructor is in a private implementation class. ieee802_15_4::preamble_tagger_cc::make is the public interface for creating new instances.

Args:
    len_preamble : "

%feature("docstring") gr::ieee802_15_4::qpsk_demapper_fi "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::qpsk_demapper_fi.

To avoid accidental use of raw pointers, ieee802_15_4::qpsk_demapper_fi's constructor is in a private implementation class. ieee802_15_4::qpsk_demapper_fi::make is the public interface for creating new instances."

%feature("docstring") gr::ieee802_15_4::qpsk_demapper_fi::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::qpsk_demapper_fi.

To avoid accidental use of raw pointers, ieee802_15_4::qpsk_demapper_fi's constructor is in a private implementation class. ieee802_15_4::qpsk_demapper_fi::make is the public interface for creating new instances."

%feature("docstring") gr::ieee802_15_4::qpsk_mapper_if "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::qpsk_mapper_if.

To avoid accidental use of raw pointers, ieee802_15_4::qpsk_mapper_if's constructor is in a private implementation class. ieee802_15_4::qpsk_mapper_if::make is the public interface for creating new instances."

%feature("docstring") gr::ieee802_15_4::qpsk_mapper_if::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::qpsk_mapper_if.

To avoid accidental use of raw pointers, ieee802_15_4::qpsk_mapper_if's constructor is in a private implementation class. ieee802_15_4::qpsk_mapper_if::make is the public interface for creating new instances."

%feature("docstring") gr::ieee802_15_4::rime_stack "

Constructor Specific Documentation:



Args:
    bc_channels : 
    uc_channels : 
    ruc_channels : 
    rime_add : "

%feature("docstring") gr::ieee802_15_4::rime_stack::make "

Constructor Specific Documentation:



Args:
    bc_channels : 
    uc_channels : 
    ruc_channels : 
    rime_add : "

%feature("docstring") gr::ieee802_15_4::zeropadding_b "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::zeropadding_b.

To avoid accidental use of raw pointers, ieee802_15_4::zeropadding_b's constructor is in a private implementation class. ieee802_15_4::zeropadding_b::make is the public interface for creating new instances.

Args:
    nzeros : "

%feature("docstring") gr::ieee802_15_4::zeropadding_b::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::zeropadding_b.

To avoid accidental use of raw pointers, ieee802_15_4::zeropadding_b's constructor is in a private implementation class. ieee802_15_4::zeropadding_b::make is the public interface for creating new instances.

Args:
    nzeros : "

%feature("docstring") gr::ieee802_15_4::zeropadding_removal_b "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::zeropadding_removal_b.

To avoid accidental use of raw pointers, ieee802_15_4::zeropadding_removal_b's constructor is in a private implementation class. ieee802_15_4::zeropadding_removal_b::make is the public interface for creating new instances.

Args:
    phr_payload_len : 
    nzeros : "

%feature("docstring") gr::ieee802_15_4::zeropadding_removal_b::make "<+description of block+>

Constructor Specific Documentation:

Return a shared_ptr to a new instance of ieee802_15_4::zeropadding_removal_b.

To avoid accidental use of raw pointers, ieee802_15_4::zeropadding_removal_b's constructor is in a private implementation class. ieee802_15_4::zeropadding_removal_b::make is the public interface for creating new instances.

Args:
    phr_payload_len : 
    nzeros : "
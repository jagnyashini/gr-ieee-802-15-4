# Install script for directory: /root/gr-ieee802-15-4/grc

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gnuradio/grc/blocks" TYPE FILE FILES
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_access_code_prefixer.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_access_code_removal_b.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_chips_to_bits_fb.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_codeword_demapper_ib.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_codeword_mapper_bi.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_codeword_soft_demapper_fb.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_deinterleaver_ff.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_dqcsk_demapper_cc.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_dqcsk_mapper_fc.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_dqpsk_mapper_ff.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_dqpsk_soft_demapper_cc.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_frame_buffer_cc.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_interleaver_ii.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_mac.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_multiuser_chirp_detector_cc.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_packet_sink.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_phr_prefixer.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_phr_removal.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_preamble_sfd_prefixer_ii.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_preamble_tagger_cc.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_qpsk_demapper_fi.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_qpsk_mapper_if.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_rime_stack.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_zeropadding_b.xml"
    "/root/gr-ieee802-15-4/grc/ieee802_15_4_zeropadding_removal_b.xml"
    )
endif()


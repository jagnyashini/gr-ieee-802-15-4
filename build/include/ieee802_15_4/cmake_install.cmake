# Install script for directory: /root/gr-ieee802-15-4/include/ieee802_15_4

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xieee802_15_4_develx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/ieee802_15_4/gnuradio/ieee802_15_4" TYPE FILE FILES
    "/root/gr-ieee802-15-4/include/ieee802_15_4/access_code_prefixer.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/access_code_removal_b.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/api.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/chips_to_bits_fb.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/codeword_demapper_ib.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/codeword_mapper_bi.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/codeword_soft_demapper_fb.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/deinterleaver_ff.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/dqcsk_demapper_cc.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/dqcsk_mapper_fc.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/dqpsk_mapper_ff.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/dqpsk_soft_demapper_cc.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/frame_buffer_cc.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/interleaver_ii.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/mac.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/multiuser_chirp_detector_cc.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/packet_sink.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/phr_prefixer.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/phr_removal.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/preamble_sfd_prefixer_ii.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/preamble_tagger_cc.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/qpsk_demapper_fi.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/qpsk_mapper_if.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/rime_stack.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/zeropadding_b.h"
    "/root/gr-ieee802-15-4/include/ieee802_15_4/zeropadding_removal_b.h"
    )
endif()


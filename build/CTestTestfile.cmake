# CMake generated Testfile for 
# Source directory: /root/gr-ieee802-15-4
# Build directory: /root/gr-ieee802-15-4/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("include/ieee802_15_4")
subdirs("lib")
subdirs("swig")
subdirs("python")
subdirs("grc")
subdirs("docs")

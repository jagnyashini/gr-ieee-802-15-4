# Install script for directory: /root/gr-ieee802-15-4/python

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xieee802_15_4_pythonx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages/ieee802_15_4" TYPE FILE FILES
    "/root/gr-ieee802-15-4/python/__init__.py"
    "/root/gr-ieee802-15-4/python/css_constants.py"
    "/root/gr-ieee802-15-4/python/css_phy.py"
    "/root/gr-ieee802-15-4/python/css_mod.py"
    "/root/gr-ieee802-15-4/python/css_demod.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xieee802_15_4_pythonx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages/ieee802_15_4" TYPE FILE FILES
    "/root/gr-ieee802-15-4/build/python/__init__.pyc"
    "/root/gr-ieee802-15-4/build/python/css_constants.pyc"
    "/root/gr-ieee802-15-4/build/python/css_phy.pyc"
    "/root/gr-ieee802-15-4/build/python/css_mod.pyc"
    "/root/gr-ieee802-15-4/build/python/css_demod.pyc"
    "/root/gr-ieee802-15-4/build/python/__init__.pyo"
    "/root/gr-ieee802-15-4/build/python/css_constants.pyo"
    "/root/gr-ieee802-15-4/build/python/css_phy.pyo"
    "/root/gr-ieee802-15-4/build/python/css_mod.pyo"
    "/root/gr-ieee802-15-4/build/python/css_demod.pyo"
    )
endif()

